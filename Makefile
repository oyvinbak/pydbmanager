.PHONY: demo test

test:
	nosetests
coverage:
	pytest --cov=dbmanager --cov-fail-under=95 tests/
coverage-report:
	pytest --cov=dbmanager --cov-fail-under=95 --cov-report=html tests/
demo:
	mkdir -p /tmp/dumps && python -m dbmanager.web dummy.yml
docker:
	docker build -t pydbmanager .

