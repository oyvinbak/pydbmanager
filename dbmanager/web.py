import os
from typing import Tuple
import flask
from flask import request, redirect, flash, g
from expiringdict import ExpiringDict
from .model import DatabaseInstance, get_database_by_name, get_databases
from .utils import timestamp
from .exceptions import ErrorException


app = flask.Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config['SECRET_KEY'] = 'the random string'

@app.errorhandler(ErrorException)
def handle_bad_request(ex):
    """Handle ErrorExceptions, printing the message"""
    return "ERROR:" + ex.message, 400


def get_database_from_args() -> Tuple[DatabaseInstance, str]:
    """Parse database name and dump name from args, and return them"""
    database = request.args.get('database')
    dumpname = request.args.get('dumpname')
    _db = get_database_by_name(database)
    return _db, dumpname


@app.route("/dump")
def dump():
    """Make a dump in a database"""
    database, dumpname = get_database_from_args()
    if not dumpname:
        dumpname = timestamp()
    database.dump(dumpname)
    flash(f"Dump {dumpname} created!")
    return redirect("/" + database.name)


@app.route("/prefix")
def set_prefix():
    """Set prefix in a database"""
    database, _ = get_database_from_args()
    prefix = request.args.get('prefix')
    database.set_prefix(prefix)
    return redirect("/" + database.name)


@app.route("/restore")
def restore():
    """Restore a dump in a database"""
    database, dumpname = get_database_from_args()
    database.restore(dumpname)
    flash(f"Dump {dumpname} restored!")
    return redirect("/" + database.name)


@app.route("/deploy")
def deploy():
    """Trigger deploy routine, dumping, set prefix and restore savepoint"""
    database, _ = get_database_from_args()
    branch = request.args.get('branch')
    database.deploy(branch)
    return redirect("/" + database.name)


@app.route("/download")
def download():
    """Download a dump"""
    database, dumpname = get_database_from_args()
    path = database.download(dumpname)
    name = os.path.basename(path)
    def iterator():
        with open(path, 'rb') as file:
            while True:
                buf = file.read(1024000)
                if not buf:
                    break
                yield buf
    headers = {'Content-Disposition':f'attachment; filename="{name}"'}
    return flask.Response(iterator(), headers=headers)


@app.route("/lock")
def set_lock():
    """Set lock for a dump in a database"""
    database, dumpname = get_database_from_args()
    lock = request.args.get('lock') == 'true'
    database.set_lock(dumpname, lock)
    return redirect("/" + database.name)


@app.route("/savepoint")
def savepoint():
    """Select savepoint for database"""
    database, dumpname = get_database_from_args()
    database.make_savepoint(dumpname)
    flash(f"Made dump {dumpname} into savepoint!")
    return redirect("/" + database.name)


@app.route("/comment")
def set_comment():
    """Set comment on a dump in a database"""
    database, dumpname = get_database_from_args()
    comment = request.args.get('comment')
    database.set_comment(dumpname, comment)
    flash("Saved comment")
    return redirect("/" + database.name)


@app.route("/delete")
def delete():
    """Delete a single dump"""
    database, dumpname = get_database_from_args()
    database.delete(dumpname)
    flash(f"Dump {dumpname} deleted")
    return redirect("/" + database.name)


@app.route("/")
def index():
    """show list of all configured databases"""
    databases = get_databases()
    return flask.render_template("index.html", databases=databases)


connection_check_cache = ExpiringDict(max_len=100, max_age_seconds=30)
def check_connection(database: DatabaseInstance):
    """Do a sanity test on a connection"""
    if database.name not in connection_check_cache:
        connection_check_cache[database.name] = database.test_connection()
    return connection_check_cache[database.name]


@app.route("/<name>")
def show_db(name):
    """Show a single db and dump info"""
    database = get_database_by_name(name)
    if not check_connection(database):
        flash(f"Connection test to '{name}' failed", category="error")
    return flask.render_template("database.html", database=database.get_info())


if __name__ == '__main__': # pragma: no cover
    app.run(debug=True)
