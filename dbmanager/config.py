import os
import re
import sys
from typing import List
from dataclasses import dataclass
import yaml
from dacite import from_dict
from dbmanager.exceptions import ErrorException
from dbmanager.folderdb import Folder

@dataclass
class DatabaseConnectionConfig:
    type: str
    hostname: str
    port: int
    username: str
    password: str
    database_names: List[str]
    drop_databases: bool = False


@dataclass
class DatabaseConfig:
    id: int
    name: str
    path: str
    connection: DatabaseConnectionConfig
    reset_name: str
    max_size: int
    max_dumps: int


@dataclass
class Config:
    databases: List[DatabaseConfig]
    dump_root: str


def check_not_blank(k, msg):
    """Raise exception is value is blank or none"""
    if hasattr(k, 'strip'):
        if not k.strip():
            raise ErrorException(msg)
    if not k:
        raise ErrorException(msg)


def get_config_from_env(environ = None) -> Config:
    """Check for certain environment variables and use that as config"""
    environ = environ or os.environ
    if 'mysql_host' in environ:
        hostname = environ.get('mysql_host')
        port = environ.get('mysql_port', 3306)
        username = environ.get('mysql_user')
        password = environ.get('mysql_password')
        databases = environ.get('mysql_databases', '').split(",")
        check_not_blank(hostname, 'hostname cant be blank')
        check_not_blank(port, 'port cant be blank')
        check_not_blank(username, 'username cant be blank')
        check_not_blank(password, 'password cant be blank')

        name = environ.get('mysql_name', hostname)

        connection = DatabaseConnectionConfig("mysql", hostname, port, username, password, databases)
        path = re.sub(r"[^\w]+", "_", name)
        database = DatabaseConfig(1, hostname, path,connection, "reset", 1000, 20)
        return Config([database], "/dump")
    return None

DEFAULT_CONFIG_SOURCES = ['/app/config.yml', '/config.yml', "config.yml"]
def find_config_file(config_sources=None):
    """Check various locations for configration files"""
    config_sources = config_sources or DEFAULT_CONFIG_SOURCES
    for file in config_sources:
        if os.path.exists(file):
            return file
    return None


def parse_config(path) -> Config:
    """Parse a config file and load it into our config structures"""
    with open(path, encoding='utf-8') as file:
        data = yaml.safe_load(file)
        result = from_dict(data_class=Config, data=data)
        return result

def load_databases() -> Config:
    """Try various options to find configuration"""
    attempts = []
    if sys.argv[-1].endswith('.yml'):
        return parse_config(sys.argv[-1])
    attempts.append("Sys argv: " + str(sys.argv))
    envconfig = get_config_from_env()
    if envconfig:
        print("Config from env variables")
        return envconfig
    attempts.append("No argv match")
    file = find_config_file()
    if not file:
        raise ErrorException("No configuration found.." + '\n'.join(attempts))
    return parse_config(file)


def get_database_by_name(name) -> DatabaseConfig:
    """Load configuration and return a datbase config by a specific name"""
    k = load_databases()
    for database in k.databases:
        if database.name == name:
            return database
    return None
