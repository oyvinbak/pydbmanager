import json
import shutil
import time
import os


class FolderException(Exception):
    def __init__(self, msg):
        self.message = msg


class JsonAttributeFile:
    def __init__(self, path):
        self.path = path

    def exists(self):
        return os.path.exists(self.path)

    def read(self):
        if not os.path.exists(self.path):
            return {}
        with open(self.path, 'r', encoding='utf-8') as file:
            return json.load(file)

    def _write(self, data):
        with open(self.path, 'w', encoding='utf-8') as file:
            json.dump(data, file)

    def get(self, key, default=None):
        return self.read().get(key, default)

    def set(self, key, value):
        data = self.read()
        data[key] = value
        self._write(data)

    def remove(self, key):
        data = self.read()
        del data[key]
        self._write(data)


class Folder:
    pass # to make typing work


class Folder:
    def __init__(self, path: str, name: str):
        self.path = path
        self.name = name
        if not os.path.isdir(self.path):
            raise FolderException(f"{self.path} does not exist")
        self.attributes = JsonAttributeFile(self._attribute_path())
        if not self.attributes.exists():
            self.attributes.set('created', time.time())
            self.attributes.set('name', self.name)
        self.created = self.attributes.get("created")

    def _get_sub_path(self, *args) -> str:
        path = os.path.abspath(os.path.join(self.path, *args))
        if not path.startswith(os.path.abspath(self.path)):
            raise FolderException(f"Not a subpath - {path} - parent: {self.path}")
        return path
    
    def _attribute_path(self) -> str:
        return self._get_sub_path("attributes.json")

    def _make_or_get_subpath(self, *args) -> str:
        path = self._get_sub_path(*args)
        if not os.path.isdir(path):
            os.makedirs(path)
        return path

    def exists(self, name: str) -> bool:
        return os.path.exists(self._get_sub_path(name))

    def delete(self):
        shutil.rmtree(self.path)

    def get_child(self, name: str):
        return Folder(self._make_or_get_subpath(name), name)

    def get_or_make_data_dir(self):
        return self._make_or_get_subpath('data')
