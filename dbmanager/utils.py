from gzip import open as gzipOpen
import datetime
import subprocess
import os
from typing import List
from dbmanager.exceptions import ErrorException


def assertIsSubdir(root, path):
    if not os.path.abspath(path).startswith(os.path.abspath(root) + os.sep):
        raise ErrorException("%r not in %r" % (path, root))


def get_size(start_path = '.'):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            # skip if it is symbolic link
            if not os.path.islink(fp):
                total_size += os.path.getsize(fp)

    return total_size

def timestamp():
    return datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")


def set_flag(path: List[str], value=""):
    flagpath = os.path.join(*path)
    if value is None:
        if os.path.exists(flagpath):
            os.unlink(flagpath)
    else:
        with open(flagpath, 'w') as f:
            f.write(value)


def read_flag(path: List[str], default=""):
    flagpath = os.path.join(*path)
    if os.path.exists(flagpath):
        with open(flagpath) as f:
            return f.read()
    else:
        return default


def readFile(path, name, default=""):
    message_file = os.path.join(path, name)
    if os.path.exists(message_file):
        return open(message_file, 'r').read()
    return default



def gzip(input, output):
    with gzipOpen(output, 'wb') as f:
        with open(input, 'rb') as dump:
            while True:
                tmp = dump.read(1024000)
                if not tmp:
                    break
                f.write(tmp)