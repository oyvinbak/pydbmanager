"""
dummy handler for tests and stuff
"""
import os
from .handler import DatabaseHandler


class DummyDatabase():
    """ dummy database """
    def __init__(self):
        self.content = "init"

    def read(self):
        """read the db value"""
        return self.content

    def write(self, value):
        """write the db value"""
        self.content = value


class DummyDatabaseHandler(DatabaseHandler):
    """Dummy database handler for tests primarily"""
    def __init__(self):
        self.database = DummyDatabase()

    def check_connection(self) -> bool:
        return True

    def backup(self, path):
        with open(os.path.join(path, 'dump.txt'), 'w', encoding='utf-8') as file:
            file.write(self.database.read())

    def restore(self, path):
        with open(os.path.join(path, 'dump.txt'), 'r', encoding='utf-8') as file:
            self.database.write(file.read())

    def download(self, path, name):
        return os.path.join(path, "dump.txt"), name + ".txt"
