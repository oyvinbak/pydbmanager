"""Interface for database handlers"""


class DatabaseHandler: # pragma: no cover
    """this is the methods you need to implement a new handler"""
    def check_connection(self) -> bool:
        """ test that we can connect """

    def backup(self, path):
        """ backup databases to a path"""

    def restore(self, path):
        """ restore contents of directory to database"""

    def download(self, path, name) -> str:
        """ return path to single file of download """
