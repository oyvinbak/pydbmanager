import os
import subprocess
from dbmanager.config import  DatabaseConnectionConfig
import io

from dbmanager.exceptions import ErrorException
from .handler import DatabaseHandler
from ..utils import gzip
from tempfile import NamedTemporaryFile

class MysqlHandler(DatabaseHandler):
    """Handler for mysql-like databases (mysql or mariadb)"""
    def __init__(self, config: DatabaseConnectionConfig):
        self.hostname = config.hostname
        self.port = config.port
        self.username = config.username
        self.password = config.password
        self.database_names = config.database_names

    def _make_connection_args(self):
        return ['-h', self.hostname,
                "--port", str(self.port),
                "-u", self.username, "--password=" + self.password]

    def _run_command(self, name, options=None, stdin=subprocess.DEVNULL, fail=True, timeout=60):
        cmd = [name, *self._make_connection_args(), *(options or [])]
        try:
            res = subprocess.run(cmd,
                           stdin=stdin, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                            timeout=timeout)
        except subprocess.TimeoutExpired as ex:
            if not fail:
                return False
            raise ErrorException("Timeout from database handler") from ex
        except subprocess.CalledProcessError as ex:
            if not fail:
                return False
            print(ex)
            print(ex.stdout)
            print(ex.stderr)
            raise ErrorException(f"Failed running command {name}") from ex
        print(res)
        if res.returncode != 0:
            print(ex)
            print(ex.stdout)
            print(ex.stderr)
            raise ErrorException("Failed creating dump")
        return True

    def check_connection(self) -> bool:
        return self._run_command("mysql", ["-e", "select 1;"], fail=False, timeout=3)

    def backup(self, path: str):
        output_file = os.path.join(path, "dump.sql")
        args = ['--lock-tables=false']
        if len(self.database_names) > 0:
            args.append("--databases")
            args.extend(self.database_names)
        args.extend([f'--result-file={output_file}'])
        self._run_command("mysqldump", args)

    def restore(self, path: str):
        self.drop_databases()
        dump_path = os.path.join(path, "dump.sql")
        self._run_command('mysql', stdin=open(dump_path,'r', encoding='utf-8'))

    def download(self, path: str, name: str):
        dump_path = os.path.join(path, "dump.sql")
        dump_path_gz = os.path.join(path, name + ".sql.gz")
        if not os.path.exists(dump_path_gz):
            gzip(dump_path, dump_path_gz)
        return dump_path_gz

    def drop_databases(self):
        with NamedTemporaryFile() as temp:
            with open(temp.name, 'w', encoding='utf-8') as f:
                drop_databases = '\n'.join(['DROP DATABASE IF EXISTS %s;' % x for x in self.database_names])
                f.write(drop_databases)
            self._run_command('mysql', stdin=open(temp.name, 'r', encoding='utf-8'))
