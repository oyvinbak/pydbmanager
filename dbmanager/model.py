import shutil
from dataclasses import dataclass
import os
import re
import datetime
from glob import glob
import time
from typing import List

from .exceptions import ErrorException
from .folderdb import Folder, JsonAttributeFile
from .handlers import DatabaseHandler, MysqlHandler, DummyDatabaseHandler
from .utils import assertIsSubdir, get_size, timestamp
from .config import Config, DatabaseConfig, load_databases, DatabaseConnectionConfig

def get_handler(connection:DatabaseConnectionConfig) -> DatabaseHandler:
    """make an appropriate handler given the connection type"""
    if connection.type == 'mysql':
        return MysqlHandler(connection)
    if connection.type == 'dummy':
        return DummyDatabaseHandler()
    raise ErrorException(f"Unknown database type '{connection.type}'")


DUMP_NAME_PATTERN = r"^[a-zA-Z_\-0-9:]+$"


@dataclass(init=False)
class DumpInfo:
    """Info about a single dump in a database"""
    path: str
    name: str
    created: str
    weeks_old: int
    success: bool
    lock: bool
    lock_timestamp: str
    savepoint: bool
    size: int
    comment: str

    def __init__(self, path):
        self.path = path
        self.attr = JsonAttributeFile(os.path.join(self.path, 'attributes.json'))
        created = self.attr.get('created', time.time())
        self.created = datetime.datetime.fromtimestamp(created)
        self.weeks_old = int(time.time() - created) // (60 * 60 * 24 * 7)
        self.name = self.attr.get('name')
        self.success = self.attr.get('success', False)
        self.lock = self.attr.get('lock', False)
        self.savepoint = self.attr.get('savepoint', False)
        self.size = round(get_size(path) / 1024 / 1024, 2)
        self.comment = self.attr.get('comment', "")

    def set_lock(self, value: bool):
        """Set lock flag to value"""
        self.attr.set('lock', value)
        self.lock = value

    def set_savepoint(self, value: bool):
        """Set savepoint flag to value"""
        self.attr.set('savepoint', value)
        self.savepoint = value

    def set_comment(self, value):
        """Set comment to value"""
        if len(value) > 1000:
            raise ErrorException("Comment too long")
        self.attr.set('comment', value)
        self.comment = value

@dataclass
class DatabaseInfo:
    """info about a database and the dumps we have stored"""
    name: str
    config: DatabaseConfig
    dumps: List[DumpInfo]
    message: str
    dump_path: str
    prefix: str


@dataclass
class DatabaseInstance:
    """A controller for the database that provides methods for dumping and restoring + utilities"""
    name: str
    config: DatabaseConfig
    dump_path: str

    def __post_init__(self):
        self.folder = Folder(self.dump_path, self.name)
        self.attr = self.folder.attributes

    def _get_path(self, *elements):
        return os.path.join(self.dump_path, *elements)

    def _make_dump_path(self, name, *extra):
        if not re.match(DUMP_NAME_PATTERN, name):
            raise ErrorException(f"dump name must match '{DUMP_NAME_PATTERN}' - '{name}'")
        path = self._get_path(name, *extra)
        assertIsSubdir(self.dump_path, path)
        return path

    def get_handler(self) -> DatabaseHandler:
        """Get a database handler for this database"""
        return get_handler(self.config.connection)

    def set_message(self, message: str):
        """Set the status message for this database"""
        self.attr.set("message", message)

    def set_prefix(self, prefix):
        """set default prefix for dumps"""
        if prefix and not prefix.endswith("_"):
            prefix += "_"
        self.attr.set("prefix", prefix)

    def get_prefix(self):
        """return the default prefix for this database"""
        return self.attr.get("prefix", "")

    def get_message(self):
        """fetch the status message for this database"""
        return self.attr.get("message", "")

    def check_has_room_for_more(self):
        """
        Check that we're not over the limits with locked dumps,
        otherwise we can assume we can clean stuff afterwards.
        """
        total_size = sum([x.size for x in self.get_dumps() if x.lock])
        if total_size >= self.config.max_size:
            raise ErrorException(f"total size locked ({total_size}) > max ({self.config.max_size})")
        if len([x for x in self.get_dumps() if x.lock]) >= self.config.max_dumps:
            raise ErrorException(f"number of dumps == max ({self.config.max_dumps}) " +
                                  "and there is none to delete")

    def cleanup(self, new=None):
        """
        Used after making a new dump to ensure we're under max count and size. Delete oldest first.
        """
        while True:
            dumps = self.get_dumps()
            total_size = sum([x.size for x in dumps])
            if len(dumps) <= self.config.max_dumps and total_size <= self.config.max_size:
                break
            for dump in reversed(dumps):
                if dump.name == new:
                    continue
                if not dump.lock:
                    shutil.rmtree(dump.path)
                    break
            else:
                break

    def test_connection(self):
        """
        Make the handler verify the connection for sanity and notification
        """
        return self.get_handler().check_connection()

    def deploy(self, branch):
        """
        Deploy procedure. Make a dump of current for safe keeping, set prefix to branch name so
        we can easily see where dumps come from, then restore the savepoint (if any).
        """
        self.dump(timestamp())
        self.set_prefix(branch)
        savepoint = self.get_save_point()
        if savepoint is not None:
            self.restore(savepoint.name)

    def dump(self, dumpname):
        """
        Make a dump from the database
        """
        self.check_has_room_for_more()
        name = self.get_prefix() + dumpname
        if not re.match(DUMP_NAME_PATTERN, name):
            raise ErrorException(f"dump name must match '{DUMP_NAME_PATTERN}' - '{name}'")
        if self.folder.exists(name):
            raise ErrorException(f"Dump already exists: '{name}'")
        child = self.folder.get_child(name)
        self.get_handler().backup(child.get_or_make_data_dir())
        child.attributes.set('success', True)
        self.cleanup(dumpname)

    def restore(self, dumpname):
        """
        Restore a dump to the database
        """
        dump = self.get_dump(dumpname)
        if not dump.success:
            raise ErrorException(f"Success flag not set on '{dumpname}'")
        try:
            self.get_handler().restore(os.path.join(dump.path, 'data'))
            now = datetime.datetime.now()
            self.set_message(f"'{dumpname}' was restored {now}")
        except Exception as ex:
            self.set_message(f"Error restoring to '{dumpname}'")
            raise ex

    def set_comment(self, dumpname, comment):
        """Set comment on dump"""
        self.get_dump(dumpname).set_comment(comment)

    def make_savepoint(self, dumpname):
        """
        Replace the savepoint if possible (dump must be successful and savepoint must be unlocked)
        """
        dump = self.get_dump(dumpname)
        if not dump.success:
            raise ErrorException(f"Success flag not set on '{dumpname}'")
        if dump.savepoint:
            return
        for adump in self.get_dumps():
            if adump.savepoint:
                adump.set_savepoint(False)
        dump.set_savepoint(True)
        dump.set_lock(True)

    def get_save_point(self) -> DumpInfo:
        """Return the dump marked as savepoint"""
        for dump in self.get_dumps():
            if dump.savepoint:
                return dump
        return None

    def delete(self, dumpname):
        """
        Delete a unlocked dump
        """
        dump = self.get_dump(dumpname)
        if dump.savepoint:
            raise ErrorException("Dump is active savepoint")
        if dump.lock:
            raise ErrorException("Dump marked as locked")
        shutil.rmtree(dump.path)

    def download(self, dumpname):
        """fetch download info from manager"""
        child = self.folder.get_child(dumpname)
        return self.get_handler().download(child.get_or_make_data_dir(), dumpname)

    def set_lock(self, dumpname, lock):
        """Set lock flag on dump"""
        self.get_dump(dumpname).set_lock(lock)

    def get_dumps(self) -> List[DumpInfo]:
        """Get info about all dumps for this db"""
        path = self._get_path("*")
        dumps = [DumpInfo(x) for x in glob(path) if os.path.isdir(x)]
        dumps.sort(key=lambda x: x.created, reverse=True)
        return dumps

    def get_dump(self, name) -> DumpInfo:
        """Get info about a specific dump"""
        if not re.match(DUMP_NAME_PATTERN, name):
            raise ErrorException(f"dump name must match '{DUMP_NAME_PATTERN}' - '{name}'")
        dumppath = self._make_dump_path(name)
        if not os.path.exists(dumppath):
            raise ErrorException(f"Dump '{name}' does not exist")
        return DumpInfo(dumppath)

    def get_info(self) -> DatabaseInfo:
        """get a dto about this database"""
        dumps = self.get_dumps()
        message = self.get_message()
        prefix = self.get_prefix()
        return DatabaseInfo(self.name, self.config, dumps, message, self.dump_path, prefix)


def get_database(database: DatabaseConfig, dump_root: str) -> DatabaseInstance:
    """Return a database dumper instance given a config and root directory"""
    if not os.path.isdir(dump_root):
        raise ErrorException(f'Dump root is not a directory: "{dump_root}"')
    database_path = os.path.join(dump_root, database.path)
    if not os.path.exists(database_path):
        os.makedirs(database_path)
    return DatabaseInstance(database.name, database, database_path)


def get_databases(config: Config=None) -> List[DatabaseInstance]:
    """Get all database dumpers given a config"""
    if config is None:
        config = load_databases()
    result = []
    for database in config.databases:
        result.append(get_database(database, config.dump_root))
    return result

def get_database_by_name(name) -> DatabaseInstance:
    """Get a single database dumper given a name"""
    databases = get_databases()
    for database in databases:
        if database.name == name:
            return database
    raise ErrorException(f"Dump '{name}' not found")
