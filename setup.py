#!/usr/bin/env python

from setuptools import setup

setup(name='pydbmanager',
      version='1.0',
      description='Tool for managing database dumps and restoration',
      packages=['dbmanager'],
      install_requires=["pyyaml",
                      "flask",
                      "dacite",
                      "gunicorn",
                      "expiringdict"],
      test_requires=["pytest", "pytest-cov", "pytest-mock"],
    )
