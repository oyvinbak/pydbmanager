import os
import tempfile
from dbmanager.exceptions import ErrorException
from dbmanager.config import Config, DatabaseConfig, check_not_blank, find_config_file, \
                            get_config_from_env, get_database_by_name, load_databases, parse_config


def eq_(a,b):
    assert a==b

def test_find_configfiles():
    with tempfile.TemporaryDirectory() as tmpdir:
        expected_path = os.path.join(tmpdir, 'oiarsgo.yml')
        with open(expected_path, 'w', encoding='utf-8') as file:
            file.write("hei")
        discover_path = find_config_file([os.path.join(tmpdir, "test.yml"),
                                        os.path.join(tmpdir, "testl.yml"),
                                        os.path.join(tmpdir, "oiarsgo.yml")])
        eq_(discover_path, expected_path)


def test_not_blank():
    try:
        check_not_blank("       ", "hei")
        assert False, "should fail"
    except ErrorException as ex:
        eq_(ex.message, "hei")
    try:
        check_not_blank(None, "hei")
        assert False, "should fail"
    except ErrorException as ex:
        eq_(ex.message, "hei")
    check_not_blank("test", "hei")
    check_not_blank(1234, "hei")


def test_config_from_env_no_value():
    eq_(get_config_from_env({}), None)


def test_config_from_env_with_env():
    dct = {}
    dct['mysql_host'] = "mysqlserver.com"
    dct["mysql_user"] = "hei"
    dct['mysql_password'] = 'passwd'
    config = get_config_from_env(dct)
    eq_(len(config.databases), 1)
    eq_(config.databases[0].name, 'mysqlserver.com')
    eq_(config.databases[0].connection.type, 'mysql')
    eq_(config.databases[0].connection.hostname, 'mysqlserver.com')
    eq_(config.databases[0].connection.username, 'hei')
    eq_(config.databases[0].connection.password, 'passwd')
    eq_(config.dump_root, '/dump')


def test_load_config():
    try:
        load_databases()
        assert False, "should fail"
    except ErrorException as ex:
        eq_(ex.message, "No configuration found..")


def test_get_database_by_name(mocker):
    load_databases = mocker.patch('dbmanager.config.load_databases')
    db = DatabaseConfig(1, "hei", "test", None, "test", 100, 100)
    load_databases.return_value = Config([db], "/dump")
    eq_(get_database_by_name("hei2"), None)
    eq_(get_database_by_name("hei"), db)


def test_parse():
    config_path = os.path.join(os.path.dirname(__file__), 'test_config.yml')
    result = parse_config(config_path)
    eq_(result.dump_root, 'dumps')
    eq_(result.databases[0].name, 'database')



def test_config_from_env(mocker):
    get_config_from_env = mocker.patch('dbmanager.config.get_config_from_env')
    get_config_from_env.return_value = Config([], "/dump")
    result = load_databases()
    eq_(result.dump_root, '/dump')
    
