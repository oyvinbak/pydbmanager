import os
import tempfile
from dbmanager.exceptions import ErrorException
from dbmanager.utils import assertIsSubdir, timestamp, set_flag, readFile, read_flag, get_size, gzip

def eq_(a,b):
    assert a==b

def test_timestamp():
    k = timestamp()
    eq_(len(k), 19)

def test_outside_dir_assert():
    try:
        assertIsSubdir("/abc/test", "/abc")
        assert False, "Assert on directories should have failed"
    except ErrorException as ex:
        eq_(ex.message, "'/abc' not in '/abc/test'")

def test_flagstuff():
    with tempfile.TemporaryDirectory() as tmpdir:
        eq_(read_flag([tmpdir, ".flag"], "nop"), "nop") # no flag set, get default
        set_flag([tmpdir, ".flag"], "data") # set flag, get value
        eq_(read_flag([tmpdir, ".flag"], "nop"), "data")
        set_flag([tmpdir, ".flag"], None) #delete flag, get default
        eq_(read_flag([tmpdir, ".flag"], "nop"), "nop")

def test_readfile():
    with tempfile.TemporaryDirectory() as tmpdir:
        eq_(readFile(tmpdir, "test", "hei"), "hei")
        with open(os.path.join(tmpdir, "test"), 'w', encoding='utf-8') as file:
            file.write("test")
        eq_(readFile(tmpdir, "test", "hei"), "test")

def test_get_size():
    with tempfile.TemporaryDirectory() as tmpdir:
        with open(os.path.join(tmpdir, "test"), 'w', encoding='utf-8') as file:
            file.write("test")
        eq_(get_size(tmpdir), 4)

def test_gzip():
    with tempfile.TemporaryDirectory() as tmpdir:
        path = os.path.join(tmpdir, "test.txt")
        output = os.path.join(tmpdir, "test.txt.gz")
        with open(path, 'w', encoding='utf-8') as file:
            file.write("abcdefgh")
        gzip(path, output)
        assert os.path.exists(output), f"{output} should exist"
