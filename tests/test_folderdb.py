import os
from tempfile import TemporaryDirectory, NamedTemporaryFile
import pytest
from pytest import raises
from dbmanager.folderdb import FolderException, JsonAttributeFile, Folder

def eq_(a,b):
    assert a==b

def test_jsonattributes():
    with TemporaryDirectory("test") as dir:
        j = JsonAttributeFile(os.path.join(dir, "attributes.json"))
        eq_(j.exists(), False)
        j.set("hei", "test")
        eq_(j.exists(), True)
        eq_(j.get('hei'), "test")
        j.remove('hei')
        eq_(j.get('hei', False), False)

def test_folder():
    with TemporaryDirectory("test") as dir:
        k = Folder(dir, 'myname')
        eq_(k.attributes.get('name'), 'myname')
        assert k.attributes.get('created', False), "created should be set"
        k.delete()
        eq_(os.path.exists(dir), False)

def test_does_not_exist():
    with raises(FolderException):
        with TemporaryDirectory("test") as dir:
            path = os.path.join(dir, 'tmp')
            k = Folder(path, 'myname')


def test_jail():
    with raises(FolderException):
        with TemporaryDirectory("test") as dir:
            k = Folder(dir, 'myname')
            k._get_sub_path("../hei")
