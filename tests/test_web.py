import tempfile
from dbmanager.exceptions import ErrorException
from dbmanager.model import DatabaseInfo
from dbmanager.web import app, check_connection

def eq_(a,b):
    assert a==b


def test_web(mocker):
    get_databases = mocker.patch('dbmanager.web.get_databases')
    get_databases.return_value = []
    tester = app.test_client()
    response = tester.get("/")
    eq_(response.status_code, 200)

def test_prefix(mocker):
    get_database_by_name = mocker.patch('dbmanager.web.get_database_by_name')
    get_database_by_name.return_value = mockdb = mocker.MagicMock()
    tester = app.test_client()
    response = tester.get("/prefix?database=test&prefix=mytest")
    eq_(response.status_code, 302)
    mockdb.set_prefix.assert_called_once_with('mytest')


def test_get_instance(mocker):
    check_connection_func = mocker.patch('dbmanager.web.check_connection')
    get_database_by_name = mocker.patch('dbmanager.web.get_database_by_name')
    get_database_by_name.return_value = mockdb = mocker.MagicMock()
    mockdb.getDatabaseInfo.return_value = DatabaseInfo("name", None, [], "","","")
    check_connection_func.return_value = False

    tester = app.test_client()
    response = tester.get("/name")
    eq_(response.status_code, 200)



def test_dump(mocker):
    get_database_by_name = mocker.patch('dbmanager.web.get_database_by_name')
    get_database_by_name.return_value = database = mocker.MagicMock()
    tester = app.test_client()
    response = tester.get("/dump?database=test&dumpname=lol")
    get_database_by_name.assert_called_once_with('test')
    database.dump.assert_called_once_with('lol')
    eq_(response.status_code, 302)

def test_dump_timestamp(mocker):
    timestamp = mocker.patch('dbmanager.web.timestamp')
    get_database_by_name = mocker.patch('dbmanager.web.get_database_by_name')
    get_database_by_name.return_value = database = mocker.MagicMock()
    timestamp.return_value = "1234"
    tester = app.test_client()
    response = tester.get("/dump?database=test&dumpname=")
    get_database_by_name.assert_called_once_with('test')
    database.dump.assert_called_once_with('1234')
    eq_(response.status_code, 302)

def test_deploy(mocker):
    get_database_by_name = mocker.patch('dbmanager.web.get_database_by_name')
    get_database_by_name.return_value = database = mocker.MagicMock()
    tester = app.test_client()
    response = tester.get("/deploy?database=test&dumpname=lol&branch=mytest")
    get_database_by_name.assert_called_once_with('test')
    database.deploy.assert_called_once_with('mytest')
    eq_(response.status_code, 302)


def test_restore(mocker):
    get_database_by_name = mocker.patch('dbmanager.web.get_database_by_name')
    get_database_by_name.return_value = database = mocker.MagicMock()
    tester = app.test_client()
    response = tester.get("/restore?database=test&dumpname=lol")
    get_database_by_name.assert_called_once_with('test')
    database.restore.assert_called_once_with('lol')
    eq_(response.status_code, 302)


def test_lock(mocker):
    get_database_by_name = mocker.patch('dbmanager.web.get_database_by_name')
    get_database_by_name.return_value = database = mocker.MagicMock()
    tester = app.test_client()
    response = tester.get("/lock?database=test&dumpname=lol&lock=true")
    get_database_by_name.assert_called_once_with('test')
    database.set_lock.assert_called_once_with('lol', True)
    eq_(response.status_code, 302)


def test_comment(mocker):
    get_database_by_name = mocker.patch('dbmanager.web.get_database_by_name')
    get_database_by_name.return_value = database = mocker.MagicMock()
    tester = app.test_client()
    response = tester.get("/comment?database=test&dumpname=lol&comment=hei")
    get_database_by_name.assert_called_once_with('test')
    database.set_comment.assert_called_once_with('lol', "hei")
    eq_(response.status_code, 302)


def test_error(mocker):
    get_database_by_name = mocker.patch('dbmanager.web.get_database_by_name')
    get_database_by_name.side_effect = ErrorException("Noe gikk galt")
    tester = app.test_client()
    response = tester.get("/comment?database=test&dumpname=lol&comment=hei")
    get_database_by_name.assert_called_once_with('test')
    eq_(response.status_code, 400)
    eq_(response.data, b"ERROR:Noe gikk galt")


def test_delete(mocker):
    get_database_by_name = mocker.patch('dbmanager.web.get_database_by_name')
    get_database_by_name.return_value = database = mocker.MagicMock()
    tester = app.test_client()
    response = tester.get("/delete?database=test&dumpname=lol")
    get_database_by_name.assert_called_once_with('test')
    database.delete.assert_called_once_with('lol')
    eq_(response.status_code, 302)


def test_savepoint(mocker):
    get_database_by_name = mocker.patch('dbmanager.web.get_database_by_name')
    get_database_by_name.return_value = database = mocker.MagicMock()
    tester = app.test_client()
    response = tester.get("/savepoint?database=test&dumpname=lol")
    get_database_by_name.assert_called_once_with('test')
    database.make_savepoint.assert_called_once_with("lol")
    eq_(response.status_code, 302)


def test_download(mocker):
    get_database_by_name = mocker.patch('dbmanager.web.get_database_by_name')
    with tempfile.NamedTemporaryFile() as file:
        with open(file.name, 'w', encoding='utf-8') as file:
            file.write("dumpcontent")
        get_database_by_name.return_value = database = mocker.MagicMock()
        database.download.return_value = file.name
        tester = app.test_client()
        response = tester.get("/download?database=test&dumpname=lol")
        get_database_by_name.assert_called_once_with('test')
        database.download.assert_called_once_with("lol")
        eq_(response.status_code, 200)
        eq_(response.data, b"dumpcontent")

def test_cached_connectiontest(mocker):
    database = mocker.MagicMock()
    database.test_connection.return_value = True
    database.name = "hei"
    check_connection(database=database)
    database.test_connection.assert_called_once()
    check_connection(database=database)
    database.test_connection.assert_called_once() # still only called once due to caching
