import os
import tempfile
from functools import wraps
import pytest
from pytest import raises
from dbmanager.folderdb import FolderException
from dbmanager.config import Config, DatabaseConfig, DatabaseConnectionConfig
from dbmanager.exceptions import ErrorException
from dbmanager.model import DatabaseInstance, get_database, get_database_by_name, get_handler

TEST_DATABASE_PATH = "testpath"

def list_dir(root, *path):
    """
    return a listdir of root+path arguments
    """
    root = os.path.abspath(root)
    return os.listdir(os.path.join(root, *path))


def get_manager(rootpath):
    """
    create a dummy database manager that uses the given path as root for tests.
    """
    dbconfig = DatabaseConnectionConfig(type="dummy", hostname="", port=1, username="", password="",
                                        database_names=["a"])
    config = DatabaseConfig(1, "test", TEST_DATABASE_PATH, dbconfig, "savepoint", 100, 2)
    return get_database(config, rootpath)

@pytest.fixture
def manager():
    """
    Provies a temporary directory for the folder management
    """
    with tempfile.TemporaryDirectory() as tmpdir:
        yield get_manager(tmpdir)


def test_dump(manager: DatabaseInstance):
    manager.check_has_room_for_more()
    manager.dump("name")
    files = list_dir(manager.dump_path, "name")
    assert files, ['attributes.json' == 'data']

    files = list_dir(manager.dump_path, "name", "data")
    assert files == ['dump.txt']
    manager.restore("name")

    message = manager.get_message()
    assert message.startswith("'name' was restored "), f"Expected message {message} to start with xx"
    info = manager.get_info()
    assert len(info.dumps) == 1



def test_get_dump_not_exist(manager):
    manager.check_has_room_for_more()
    with pytest.raises(ErrorException, match=r"Dump 'name' does not exist"):
        manager.get_dump("name")



def test_get_dump_invalid_name(manager):
    manager.check_has_room_for_more()
    with pytest.raises(ErrorException, match="dump name must match"):
        manager.get_dump("name.....")



def test_no_savepoint_gives_none(manager):
    manager.dump("test")
    assert manager.get_save_point() == None


def test_dump_fail(manager: DatabaseInstance, mocker):
    manager.check_has_room_for_more()
    mock_handler = mocker.MagicMock()
    manager.get_handler = lambda: mock_handler
    mock_handler.backup.side_effect = ErrorException("feil")

    with raises(ErrorException) as cm:
        manager.dump("name")
    assert cm.value.message == "feil"

    dump = manager.get_dump("name")
    assert dump.success == False
    with raises(ErrorException) as cm:
        manager.make_savepoint("name")
    assert cm.value.message == "Success flag not set on 'name'"
    
    with raises(ErrorException) as cm:
        manager.restore("name")
    assert cm.value.message == "Success flag not set on 'name'"



def test_restore_fail(manager: DatabaseInstance, mocker):
    manager.check_has_room_for_more()
    manager.dump("name")

    mock_handler = mocker.MagicMock()
    manager.get_handler = lambda: mock_handler
    mock_handler.restore.side_effect = ErrorException("feil")
    with raises(ErrorException) as cm:
        manager.restore("name")
    assert cm.value.message == "feil"



def test_dump_invalid_name(manager):
    manager.check_has_room_for_more()
    with raises(ErrorException) as cm:
        manager.dump("/../name/")
    assert cm.value.message == "dump name must match '^[a-zA-Z_\\-0-9:]+$' - '/../name/'"



def testdownload(manager):
    manager.check_has_room_for_more()
    manager.dump("name")
    path, name = manager.download("name")
    assert name == "name.txt"
    expected_path = os.path.join(manager.dump_path, "name", "data", "dump.txt")
    assert path == expected_path
    assert os.path.exists(expected_path)


def test_comment(manager: DatabaseInstance):
    manager.check_has_room_for_more()
    manager.dump("name")
    manager.set_comment("name", "comment value")
    assert manager.get_dump("name").comment == "comment value"
    with raises(ErrorException, match="Comment too long"):
        manager.set_comment("name", "comment value" * 100)


def test_connection(manager):
    manager.check_has_room_for_more()
    manager.test_connection()



def test_prefix(manager):
    manager.check_has_room_for_more()
    manager.set_prefix("hei_")
    manager.dump("name")
    files = list_dir(manager.dump_path)
    assert files, ['attributes.json' == 'hei_name']
    files = list_dir(manager.dump_path, "hei_name")
    assert files, ['attributes.json' == 'data']



def test_dump_already_exists(manager):
    manager.check_has_room_for_more()
    manager.dump("name")
    with raises(ErrorException) as cm:
        manager.dump("name")
    assert cm.value.message == "Dump already exists: 'name'"



def test_restore_doesnt_exists(manager):
    try:
        manager.restore("name")
    except ErrorException as ex:
        assert ex.message == "Dump 'name' does not exist"



def test_dump_delete(manager):
    manager.check_has_room_for_more()
    manager.dump("name")
    files = list_dir(manager.dump_path, "name")
    assert files, ["attributes.json" == "data"]
    manager.delete("name")
    files = list_dir(manager.dump_path)
    assert files == ["attributes.json"]



def test_dump_lock(manager):
    manager.check_has_room_for_more()
    manager.dump("name")

    # lock dump
    manager.set_lock("name", True)
    with raises(ErrorException) as cm:
        manager.delete("name")
    assert cm.value.message == "Dump marked as locked"

    # unlock and delete
    manager.set_lock("name", False)
    manager.delete("name")
    files = list_dir(manager.dump_path)
    assert files == ["attributes.json"]



def test_cleanup(manager):
    manager.check_has_room_for_more()
    manager.dump("name")
    manager.dump("name2")
    manager.dump("name3")
    manager.dump("name4")
    dumps = manager.get_dumps()
    assert len(dumps) == 2



def test_cleanup_lock_and_no_more_room(manager):
    manager.dump("anothername")
    manager.set_lock("anothername", True)
    manager.dump("name")
    manager.set_lock("name", True)

    with raises(ErrorException) as cm:
        manager.dump("name2")
    assert cm.value.message == "number of dumps == max (2) and there is none to delete"
    manager.set_lock("name", False)
    manager.dump("name2")



def test_cleanup_lock_and_no_more_space(manager, mocker):
    get_size = mocker.patch("dbmanager.model.get_size")
    get_size.return_value = 1
    manager.dump("anothername")
    manager.set_lock("anothername", True)
    get_size.return_value = 1000000000000
    with raises(ErrorException) as cm:
        manager.dump("name")
    assert cm.value.message == "total size locked (953674.32) > max (100)"
    manager.set_lock("anothername", False)
    manager.dump("name")


def test_savepoint(manager: DatabaseInstance):
    manager.dump("name")
    manager.make_savepoint("name")
    files = list_dir(manager.dump_path)
    assert files, ["name" == "attributes.json"]
    files = list_dir(manager.dump_path, "name")
    assert files, ["attributes.json" == "data"]


def test_savepoint_delete(manager: DatabaseInstance):
    manager.dump("name")
    manager.make_savepoint("name")
    with raises(ErrorException) as cm:
        manager.delete("name")
    assert cm.value.message == "Dump is active savepoint"


def test_safepoint_replace(manager: DatabaseInstance):
    manager.dump("name")
    manager.dump("name2")
    manager.make_savepoint("name")
    manager.make_savepoint("name")
    assert manager.get_dump("name").savepoint == True
    manager.make_savepoint("name2")
    assert manager.get_dump("name").savepoint == False
    assert manager.get_dump("name2").savepoint == True


def test_deploy(manager, mocker):
    timestamp = mocker.patch("dbmanager.model.timestamp")
    timestamp.return_value = "1234"
    manager.dump("mytest")
    manager.make_savepoint("mytest")
    manager.deploy("test")
    assert manager.get_prefix() == 'test_'
    files = list_dir(manager.dump_path)
    assert files, ['attributes.json', '1234' == 'mytest']
    timestamp.return_value = "1235"
    manager.deploy("anotherdeploy")
    assert manager.get_prefix() == 'anotherdeploy_'
    files = list_dir(manager.dump_path)
    assert files, ['test_1235', 'attributes.json' == 'mytest']


def test_unknown_handler():
    with raises(ErrorException) as cm:
        get_handler(DatabaseConnectionConfig("invalid", "hei", 123, "hei", "hei", []))
    assert cm.value.message == "Unknown database type 'invalid'"


def test_get_database_by_name(mocker):
    load_databases = mocker.patch('dbmanager.model.load_databases')
    with tempfile.TemporaryDirectory() as tmpdir:
        dbconfig = DatabaseConfig(1, "hei", "test", None, "test", 100, 100)
        load_databases.return_value = Config([dbconfig], tmpdir)
        assert get_database_by_name("hei").name == "hei"
        with raises(ErrorException) as cm:
            get_database_by_name("hei2")
        assert cm.value.message == "Dump 'hei2' not found"
