import os
import subprocess
import tempfile
from dataclasses import dataclass
from dbmanager.model import get_handler
from dbmanager.config import DatabaseConnectionConfig
from dbmanager.exceptions import ErrorException
from dbmanager.handlers import MysqlHandler

def eq_(a,b):
    assert a==b

def get_test_handler() -> MysqlHandler:
    """Make a mysql handler for the tests"""
    config = DatabaseConnectionConfig("mysql", "hostname", "3306", "admin", "admin", ["hei"])
    return MysqlHandler(config)


@dataclass
class Return:
    returncode: int
    stdout: str
    stderr: str


def test_connection_test_ok(mocker):
    run = mocker.patch('subprocess.run')
    run.return_value = Return(0, "<stdout>", "<stderr>")
    mysql = get_test_handler()
    eq_(mysql.check_connection(), True)
    run.assert_called_with(['mysql', '-h', 'hostname', '--port', '3306',
                            '-u', 'admin', '--password=admin', '-e', 'select 1;'],
                            stdin=-3, stdout=-1, stderr=-1, check=True, timeout=3)


def test_connection_test_fail(mocker):
    run = mocker.patch('subprocess.run')
    run.side_effect = subprocess.CalledProcessError(1, None)
    mysql = get_test_handler()
    eq_(mysql.check_connection(), False)
    run.assert_called_with(['mysql', '-h', 'hostname', '--port', '3306',
                            '-u', 'admin', '--password=admin', '-e', 'select 1;'],
                            stdin=-3, stdout=-1, stderr=-1, check=True, timeout=3)

def test_cmd_error(mocker):
    run = mocker.patch('subprocess.run')
    run.side_effect = subprocess.CalledProcessError(1, None)
    mysql = get_test_handler()
    try:
        mysql.backup("test")
        assert False, "running backup should fail"
    except ErrorException as ex:
        eq_(ex.message, "Failed running command mysqldump")


def test_mysql_backup(mocker):
    run = mocker.patch('subprocess.run')
    run.return_value = Return(0, "", "")
    mysql = get_test_handler()
    mysql.backup("test")
    run.assert_called_with(['mysqldump', '-h', 'hostname', '--port', '3306',
        '-u', 'admin', '--password=admin',
        '--databases', 'hei', '--result-file=test/dump.sql'],
        stdin=-3, stdout=-1, stderr=-1, check=True, timeout=15)


def test_mysql_backup_timeout(mocker):
    run = mocker.patch('subprocess.run')
    run.side_effect = subprocess.TimeoutExpired("hei", 100)
    mysql = get_test_handler()
    try:
        mysql.backup("test")
        assert False, "Timeout expired should throw"
    except ErrorException as ex:
        eq_(ex.message, "Timeout from database handler")
    eq_(mysql.check_connection(), False)

def test_mysql_restore(mocker):
    run = mocker.patch('subprocess.run')
    with tempfile.TemporaryDirectory() as tmpdir:
        run.return_value = Return(0, "", "")
        mysql = get_test_handler()
        with open(os.path.join(tmpdir, 'dump.sql'), 'w', encoding='utf-8') as file:
            file.write("test")
        mysql.restore(tmpdir)
        run.assert_called_with(['mysql', '-h', 'hostname', '--port', '3306',
                                '-u', 'admin', '--password=admin'],
                                stdin=mocker.ANY, stdout=-1, stderr=-1, check=True, timeout=15)

def test_mysql_download():
    with tempfile.TemporaryDirectory() as tmpdir:
        mysql = get_test_handler()
        with open(os.path.join(tmpdir, 'dump.sql'), 'w', encoding='utf-8') as file:
            file.write("")
        result = mysql.download(tmpdir, "pretty")
        eq_(result, os.path.join(tmpdir, "pretty.sql.gz"))


def test_get_handler_mysql(mocker):
    run = mocker.patch('subprocess.run')
    config = DatabaseConnectionConfig("mysql", "mysql", 3306, "admin", "admin", ["admin"])
    result = get_handler(config)
    run.return_value = Return(0, "", "")
    result.check_connection()
