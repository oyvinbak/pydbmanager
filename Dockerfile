from python:slim-bullseye
run apt-get update
run apt-get install -y default-mysql-client

add requirements.txt /app/requirements.txt
run pip install -r /app/requirements.txt

add dbmanager /app/dbmanager

RUN useradd -ms /bin/false dbmanager
user dbmanager
workdir /app
entrypoint ["gunicorn", "-b", "0.0.0.0:9100", "dbmanager.web:app"]
